#ifndef __CPEKU_TIME_H__
#define __CPEKU_TIME_H__

#include <map>
#include "driver.h"
#include "device.h"
#include "esp_log.h"

class CPEKU_Time : public Device {
  public:

    CPEKU_Time();

    void init(void) override;
    void process(Driver *drv) override;
    int prop_count(void) override { return 0; }
    bool prop_name(int index, char *name) override { return false; }
    bool prop_unit(int index, char *unit) override { return false; }
    bool prop_attr(int index, char *attr) override { return false; }
    bool prop_read(int index, char *value) override { return false; }
    bool prop_write(int index, char *value) override { return false; }

    double uptime();

  private:
    enum state_t {
      DETECT,
      RUNNING,
    };
    static state_t _state;
};

#endif
