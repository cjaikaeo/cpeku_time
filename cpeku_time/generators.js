Blockly.JavaScript['cpeku_time_uptime'] = function(block) {
  var code = 'DEV_IO.CPEKU_Time().uptime()' ;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};
