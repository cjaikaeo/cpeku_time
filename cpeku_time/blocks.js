Blockly.Blocks['cpeku_time_uptime'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(Blockly.Msg.CPEKU_TIME_UPTIME);
    this.setOutput(true, "Number");
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_TIME_UPTIME_TOOLTIP);
    this.setHelpUrl("");
  }
};
