#include <functional>
#include <map>

#include <esp_log.h>
#include <esp_timer.h>
#include "kidbright32.h"
#include "cpeku_time.h"

static const char *TAG = "CPEKU_Time";
CPEKU_Time::state_t CPEKU_Time::_state;

CPEKU_Time::CPEKU_Time() {
}

void CPEKU_Time::init(void) {
  _state = DETECT;
}

void CPEKU_Time::process(Driver *drv) {
  switch (_state) {
    case DETECT:
      // clear error flag
      error = false;
      // set initialized flag
      initialized = true;
      break;
    case RUNNING:
      break;
  }
}

double CPEKU_Time::uptime() {
  return static_cast<double>(esp_timer_get_time()/1e6);
}
